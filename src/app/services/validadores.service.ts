import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidadoresService {

  constructor() { }

  noMayuscula( control: FormControl): {[s:string]: boolean } {
    if ( control.value?.toLowercase() === 'ass' ) {
      return {
        noMayuscula: true
        }
      }
    return null;
    }
}
