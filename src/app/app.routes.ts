import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LandingComponent } from './components/landing/landing.component';
import { LoginComponent } from './components/login/login.component';
import { RegistroEncargadoComponent } from './components/registro-encargado/registro-encargado.component';
import { RegistroDepartamentosComponent } from './components/registro-departamentos/registro-departamentos.component';
import { RegistroSupermercadoComponent } from './components/registro-supermercado/registro-supermercado.component';
import { RegistroTrabajadoresComponent } from './components/registro-trabajadores/registro-trabajadores.component';
import { RegistroComponent } from './components/registro/registro.component';

const APP_ROUTES: Routes = [
   { path: 'home', component: HomeComponent },
   { path: 'landing', component: LandingComponent },
   { path: 'landing/registro-encargado', component: RegistroEncargadoComponent },
   { path: 'landing/registro-departamentos', component: RegistroDepartamentosComponent },
   { path: 'landing/registro-supermercado', component: RegistroSupermercadoComponent },
   { path: 'landing/registro-trabajadores', component: RegistroTrabajadoresComponent },
   { path: 'login', component: LoginComponent },
   { path: 'registro', component: RegistroComponent },
   { path: '**', pathMatch: 'full', redirectTo: 'home' }
]

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);