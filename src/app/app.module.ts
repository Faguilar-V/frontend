import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import  { FormsModule, ReactiveFormsModule } from '@angular/forms';
import{ HttpClientModule } from '@angular/common/http';

//Routes
import { APP_ROUTING } from './app.routes';

//Components
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LandingComponent } from './components/landing/landing.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { EncargadoComponent } from './components/encargado/encargado.component';
import { DatosSupermercadoComponent } from './components/datos-supermercado/datos-supermercado.component';
import { RegistroEncargadoComponent } from './components/registro-encargado/registro-encargado.component';
import { RegistroSupermercadoComponent } from './components/registro-supermercado/registro-supermercado.component';
import { RegistroDepartamentosComponent } from './components/registro-departamentos/registro-departamentos.component';
import { RegistroTrabajadoresComponent } from './components/registro-trabajadores/registro-trabajadores.component';
import { RegistroComponent } from './components/registro/registro.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LandingComponent,
    LoginComponent,
    NavbarComponent,
    EncargadoComponent,
    DatosSupermercadoComponent,
    RegistroEncargadoComponent,
    RegistroSupermercadoComponent,
    RegistroDepartamentosComponent,
    RegistroTrabajadoresComponent,
    RegistroComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
