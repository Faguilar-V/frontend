import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { GuardsCheckStart } from '@angular/router';

@Component({
  selector: 'app-registro-departamentos',
  templateUrl: './registro-departamentos.component.html',
  styleUrls: ['./registro-departamentos.component.css']
})
export class RegistroDepartamentosComponent implements OnInit {

  forma: FormGroup;

  constructor(private fb: FormBuilder) {
    this.crearFormulario();
  }

  ngOnInit(): void {
  }

  get ndepartamentoNoValido() {
    return this.forma.get('ndepartamento').invalid && this.forma.get('ndepartamento').touched
  }
  get trabajadores() {
    return this.forma.get('trabajadores') as FormArray;
  }

  crearFormulario() {

    this.forma = this.fb.group({
      ndepartamento   : ['', Validators.required],
      trabajadores    : this.fb.array([
      ])
  });
}

  agregarTrabajador() {
    this.trabajadores.push( this.fb.control('', Validators.required)  );
  }
  borrarTrabajador(i: number) {
    this.trabajadores.removeAt(i);
  }
  guardar() {
    console.log( this.forma );

    if ( this.forma.invalid ) {
      return Object.values( this.forma.controls ).forEach( control => {
        if ( control instanceof FormGroup ) {
          Object.values( control.controls ).forEach( control => control.markAsTouched() );
        }
        else{
          control.markAsTouched();
        }
      });
    }
  }

}
