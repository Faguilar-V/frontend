import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GuardsCheckStart } from '@angular/router';

@Component({
  selector: 'app-registro-supermercado',
  templateUrl: './registro-supermercado.component.html',
  styleUrls: ['./registro-supermercado.component.css']
})
export class RegistroSupermercadoComponent implements OnInit {

  forma: FormGroup;

  constructor(private fb: FormBuilder) {
    this.crearFormulario();
  }

  ngOnInit(): void {
  }

  get nombreNoValido() {
    return this.forma.get('nombre').invalid && this.forma.get('nombre').touched
  }
  get direccionNoValido() {
    return this.forma.get('direccion').invalid && this.forma.get('direccion').touched
  }
  get rsNoValido() {
    return this.forma.get('razonsocial').invalid && this.forma.get('razonsocial').touched
  }
  get correoNoValido() {
    return this.forma.get('correo').invalid && this.forma.get('correo').touched
  }
  get telefonoNoValido() {
    return this.forma.get('telefono').invalid && this.forma.get('telefono').touched
  }
  get passNoValido() {
    return this.forma.get('pass').invalid && this.forma.get('pass').touched;
  }
  get confpassNoValido() {
    const pass = this.forma.get( 'pass' ).value;
    const confpass = this.forma.get( 'confpass' ).value;
    return ( pass === confpass ) ? false : true;
  }



  get conNoValido() {
    return this.forma.get('contrasena.original').invalid && this.forma.get('contrasena.original').touched
  }
  get confconNoValido() {
    return this.forma.get('contrasena.confirmacion').invalid && this.forma.get('contrasena.confirmacion').touched
  }


  crearFormulario() {

    this.forma = this.fb.group({
      nombre          : ['', [Validators.required, Validators.minLength(5)]],
      direccion       : ['', [Validators.required, Validators.minLength(5)]],
      razonsocial    : ['', [Validators.required, Validators.minLength(5)]],
      correo          : ['', [Validators.required, Validators.email]],
      telefono        : ['',[Validators.required]],
      pass            : ['', [Validators.required, Validators.minLength(8)]],
      confpass        : ['', [Validators.required]],
      contrasena      : this.fb.group({
        original      : ['', [Validators.required]],
        confirmacion  : ['', [Validators.required]],
      })
  });
}
  guardar() {
    console.log( this.forma );

    if ( this.forma.invalid ) {
      return Object.values( this.forma.controls ).forEach( control => {
        if ( control instanceof FormGroup ) {
          Object.values( control.controls ).forEach( control => control.markAsTouched() );
        }
        else{
          control.markAsTouched();
        }
      });
    }
  }

}
