import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GuardsCheckStart } from '@angular/router';

@Component({
  selector: 'app-registro-trabajadores',
  templateUrl: './registro-trabajadores.component.html',
  styleUrls: ['./registro-trabajadores.component.css']
})
export class RegistroTrabajadoresComponent implements OnInit {

  forma: FormGroup;

  constructor(private fb: FormBuilder) {
    this.crearFormulario();
  }

  ngOnInit(): void {
  }

  get nombreNoValido() {
    return this.forma.get('nombre').invalid && this.forma.get('nombre').touched
  }
  get apellidosNoValido() {
    return this.forma.get('apellidos').invalid && this.forma.get('apellidos').touched
  }
  get diasNoValido() {
    return this.forma.get('diaslab').invalid && this.forma.get('diaslab').touched
  }
  get telefonoNoValido() {
    return this.forma.get('telefono').invalid && this.forma.get('telefono').touched
  }
  get idNoValido() {
    return this.forma.get('id').invalid && this.forma.get('id').touched
  }

  crearFormulario() {

    this.forma = this.fb.group({
      nombre          : ['', [Validators.required, Validators.minLength(5)]],
      apellidos       : ['', [Validators.required, Validators.minLength(5)]],
      diaslab        : ['', [Validators.required, Validators.min(1) , Validators.max(7)]],
      telefono        : ['',[Validators.required,Validators.minLength(1000000000), Validators.maxLength(9999999999)]],
      id              : ['',[Validators.required]],
    });
}
  guardar() {
    console.log( this.forma );

    if ( this.forma.invalid ) {
      return Object.values( this.forma.controls ).forEach( control => {
        if ( control instanceof FormGroup ) {
          Object.values( control.controls ).forEach( control => control.markAsTouched() );
        }
        else{
          control.markAsTouched();
        }
      });
    }
  }

}
