import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosSupermercadoComponent } from './datos-supermercado.component';

describe('DatosSupermercadoComponent', () => {
  let component: DatosSupermercadoComponent;
  let fixture: ComponentFixture<DatosSupermercadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosSupermercadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosSupermercadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
