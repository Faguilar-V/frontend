import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GuardsCheckStart } from '@angular/router';
import { ValidadoresService } from '../../services/validadores.service';

@Component({
  selector: 'app-registro-encargado',
  templateUrl: './registro-encargado.component.html',
  styleUrls: ['./registro-encargado.component.css']
})
export class RegistroEncargadoComponent implements OnInit {
  forma: FormGroup;

  constructor(private fb: FormBuilder,
              private validadores: ValidadoresService) {
    this.crearFormulario();
  }

  ngOnInit(): void {
  }

  get nombreNoValido() {
    return this.forma.get('nombre').invalid && this.forma.get('nombre').touched
  }
  get apellidosNoValido() {
    return this.forma.get('apellidos').invalid && this.forma.get('apellidos').touched
  }
  get correoNoValido() {
    return this.forma.get('correo').invalid && this.forma.get('correo').touched
  }
  get telefonoNoValido() {
    return this.forma.get('telefono').invalid && this.forma.get('telefono').touched
  }
  get fechaNoValido() {
    return this.forma.get('fecha').invalid && this.forma.get('fecha').touched
  }
  get genNoValido() {
    return this.forma.get('genero').invalid && this.forma.get('genero').touched
  }
  get passNoValido() {
    return this.forma.get('pass').invalid && this.forma.get('pass').touched;
  }
  get confpassNoValido() {
    const pass = this.forma.get( 'pass' ).value;
    const confpass = this.forma.get( 'confpass' ).value;
    return ( pass === confpass ) ? false : true;
  }



  get conNoValido() {
    return this.forma.get('contrasena.original').invalid && this.forma.get('contrasena.original').touched
  }
  get confconNoValido() {
    return this.forma.get('contrasena.confirmacion').invalid && this.forma.get('contrasena.confirmacion').touched
  }


  crearFormulario() {

    this.forma = this.fb.group({
      nombre          : ['', [Validators.required, Validators.minLength(5)]],
      apellidos       : ['', [Validators.required, Validators.minLength(5)]],
      correo          : ['', [Validators.required, Validators.email]],
      telefono        : ['',[Validators.required, Validators.minLength(1000000000), Validators.maxLength(9999999999)]],
      fecha           : ['', [Validators.required]],
      genero          : ['',[Validators.required]],
      pass            : ['', [Validators.required, Validators.minLength(8)]],
      confpass       : ['', [Validators.required]],
  });
}
  guardar() {
    console.log( this.forma );

    if ( this.forma.invalid ) {
      return Object.values( this.forma.controls ).forEach( control => {
        if ( control instanceof FormGroup ) {
          Object.values( control.controls ).forEach( control => control.markAsTouched() );
        }
        else{
          control.markAsTouched();
        }
      });
    }
  }

}
